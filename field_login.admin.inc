<?php
/**
 * @file
 *  Administration functions for Field Login module.
 */

function field_login_main_settings() {
  $form = array();

  module_load_include('inc', 'field_ui', 'field_ui.admin');
  $entity_type = 'user';
  $bundle      = 'user';

  field_ui_inactive_message($entity_type, $bundle);

  // When displaying the form, make sure the list of fields is up-to-date.
  if (empty($form_state['post'])) {
    field_info_cache_clear();
  }

  // Gather bundle information.
  $instances = field_info_instances($entity_type, $bundle);

  $options = array();

  foreach($instances as $key => $instance) {
    $options[$key] = $instance['label'];
  }

  $form['field_login_fields'] = array(
    '#type'    => 'checkboxes',
    '#options' => $options,
    '#default_value' => variable_get('field_login_fields', array()),
    '#title'   => t('Allowed Fields'),
    '#description' => t('Select the fields that are valid for login.'),
  );

  return system_settings_form($form);
}